﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultKeysConverter.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.T9Helper
{
    using System.Collections.Generic;

    using Keypad;

    /// <inheritdoc />
    public sealed class DefaultKeysConverter : IKeysConverter
    {
        /// <inheritdoc />
        public IReadOnlyDictionary<char, string> Convert(IReadOnlyCollection<IKey> keys)
        {
            var map = new Dictionary<char, string>();

            foreach (IKey key in keys)
            {
                string translatedKey = string.Empty;

                foreach (char character in key.Characters)
                {
                    translatedKey += key.Value;
                    map.Add(character, translatedKey);
                }
            }

            return map;
        }
    }
}