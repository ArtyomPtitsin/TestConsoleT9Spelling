﻿namespace TestConsoleT9Spelling.T9Helper
{
    using System.Collections.Generic;

    using Keypad;

    /// <summary>
    /// Converter keys with characters to the map of character and keys sequence.
    /// </summary>
    public interface IKeysConverter
    {
        /// <summary>
        ///     Convert keys with characters to the map character and keys sequence.
        /// </summary>
        /// <param name="keys">The Keys with characters</param>
        /// <returns>The map of character and keys sequence.</returns>
        IReadOnlyDictionary<char, string> Convert(IReadOnlyCollection<IKey> keys);
    }
}