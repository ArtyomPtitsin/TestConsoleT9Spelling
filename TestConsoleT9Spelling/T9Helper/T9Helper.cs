﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="T9Helper.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.T9Helper
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Keypad;

    /// <inheritdoc />
    public sealed class T9Helper : IT9Helper
    {
        /// <summary>
        ///     The map of character and the it key.
        /// </summary>
        private readonly IDictionary<char, char> _characterAndKey;

        /// <summary>
        ///     The map of character and sequence of the tapping by keys for getting its.
        /// </summary>
        private readonly IReadOnlyDictionary<char, string> _characterAndSequenceKeys;

        /// <summary>
        ///     The keys.
        /// </summary>
        private readonly IReadOnlyCollection<IKey> _keys;

        /// <summary>
        ///     The pause between tapping to the same button for getting the next character.
        /// </summary>
        private readonly char _pause;

        /// <summary>
        ///     The T9 helper.
        /// </summary>
        /// <param name="keys">The keys of keypad.</param>
        /// <param name="converterKeys">Converter keys to the map of character and sequence of the tapping by keys for getting its.</param>
        /// <param name="pause">The pause between tapping to the same button for getting the next character.</param>
        public T9Helper(IReadOnlyCollection<IKey> keys,
                        Converter<IReadOnlyCollection<IKey>, IReadOnlyDictionary<char, string>> converterKeys,
                        char pause = ' '
        )
        {
            _keys = keys;
            _pause = pause;
            _characterAndSequenceKeys = converterKeys.Invoke(keys);

            _characterAndKey = ConvertKeysToCharacterKeyMap();
        }

        /// <inheritdoc />
        /// <exception cref="System.NotSupportedException">Thrown if not character is supported.</exception>
        public string Translate(string text)
        {
            var translatedTextStringBuilder = new StringBuilder();
            char? memKey = null;

            foreach (char character in text)
            {
                if (!_characterAndKey.TryGetValue(character, out char key))
                {
                    throw new NotSupportedException($"Character \"{character}\" not supported in the T9 helper.");
                }

                string translatedChar = TranslateCharacter(character);
                translatedTextStringBuilder.Append(!Equals(key, memKey) ? translatedChar : _pause + translatedChar);
                memKey = key;
            }

            return translatedTextStringBuilder.ToString();
        }

        /// <summary>
        ///     Translate character to the key sequence.
        /// </summary>
        /// <param name="character">The character.</param>
        /// <returns>The keys sequence.</returns>
        /// <exception cref="System.NotSupportedException">Thrown if not character is supported.</exception>
        private string TranslateCharacter(char character)
        {
            if (!_characterAndSequenceKeys.TryGetValue(character, out string keySequence))
            {
                throw new NotSupportedException($"Character \"{character}\" not supported in the T9 helper.");
            }

            return keySequence;
        }

        /// <summary>
        ///     Convert keys to the map of character and the it key.
        /// </summary>
        /// <returns>The map of character and the it key value.</returns>
        private IDictionary<char, char> ConvertKeysToCharacterKeyMap()
        {
            var characterAndKey = new Dictionary<char, char>();

            foreach (IKey key in _keys)
            foreach (char character in key.Characters)
                characterAndKey.Add(character, key.Value);

            return characterAndKey;
        }
    }
}