﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IT9Helper.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.T9Helper
{
    /// <summary>
    ///     T9 helper.
    /// </summary>
    public interface IT9Helper
    {
        /// <summary>
        ///     Translate text in the pressed keys sequence.
        /// </summary>
        /// <param name="text">Text for translate.</param>
        /// <returns>String consisting from the pressed keys sequence.</returns>
        string Translate(string text);
    }
}