﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidationHelper.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling
{
    using System;

    using Validator;

    /// <summary>
    ///     Contains the input validator methods.
    /// </summary>
    public sealed class ValidationHelper
    {
        /// <summary>
        /// The validator factory.
        /// </summary>
        private readonly IValidatorFactory _validatorFactory;

        /// <inheritdoc />
        public ValidationHelper(IValidatorFactory validatorFactory)
        {
            _validatorFactory = validatorFactory;
        }

        /// <summary>
        ///     Validate case count.
        /// </summary>
        /// <returns>The case count validator.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        ///     Thrown when case count is beyond the permissible limits.
        /// </exception>
        public void ValidateCaseCount(int count, int minCount, int maxCount)
        {
            IValidator<int> minValidator = _validatorFactory.CreateMinValidator(minCount);
            IValidator<int> maxValidator = _validatorFactory.CreateMaxValidator(maxCount);

            IValidatorHasErrorMessage<int> validator = _validatorFactory.CreateCaseCountValidator(minValidator, maxValidator);

            if (!validator.Validate(count))
            {
                throw new ArgumentOutOfRangeException(validator.ErrorMessage);
            }
        }

        /// <summary>
        ///     Validate message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="minLength">The min length of characters.</param>
        /// <param name="maxLength">The max length of characters.</param>
        /// <returns>The message validator.</returns>
        /// <exception cref="System.ArgumentException">
        ///     Thrown when length of message in characters is beyond the permissible limits.
        /// </exception>
        public void ValidateMessage(string message, int minLength, int maxLength)
        {
            IValidator<int> minValidator = _validatorFactory.CreateMinValidator(minLength);
            IValidator<int> maxValidator = _validatorFactory.CreateMaxValidator(maxLength);
            IValidatorHasErrorMessage<string> validator = _validatorFactory.CreateMessageValidator(minValidator, maxValidator);

            if (!validator.Validate(message))
            {
                throw new ArgumentOutOfRangeException(validator.ErrorMessage);
            }
        }
    }
}