﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageValidator.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.Validator
{
    /// <summary>
    ///     The message validator.
    /// </summary>
    public sealed class MessageValidator : IValidatorHasErrorMessage<string>
    {
        /// <summary>
        ///     The max value validator.
        /// </summary>
        private readonly IValidator<int> _maxValueValidator;

        /// <summary>
        ///     The min value validator.
        /// </summary>
        private readonly IValidator<int> _minValueValidator;

        /// <inheritdoc />
        public string ErrorMessage { get; private set; } = string.Empty;

        /// <inheritdoc />
        public string ValidationTarget { get; private set; }

        /// <summary>
        ///     Create message validator.
        /// </summary>
        /// <param name="minValidator">The min length of message in characters.</param>
        /// <param name="maxValidator">The max length of message in characters.</param>
        public MessageValidator(IValidator<int> minValidator, IValidator<int> maxValidator)
        {
            _minValueValidator = minValidator;
            _maxValueValidator = maxValidator;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Valildate message.
        /// </summary>
        /// <param name="message">The message for validate.</param>
        /// <returns>True if validation is success. Otherwise false.</returns>
        public bool Validate(string message)
        {
            ValidationTarget = message;

            if (!_minValueValidator.Validate(message.Length))
            {
                ErrorMessage = $"The length of message in characters must be more or equals {_minValueValidator.ValidationTarget}";

                return false;
            }

            if (!_maxValueValidator.Validate(message.Length))
            {
                ErrorMessage = $"The length of message in characters must be less or equals {_maxValueValidator.ValidationTarget}";

                return false;
            }

            return true;
        }
    }
}