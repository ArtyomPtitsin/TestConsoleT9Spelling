﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IValidatorHasErrorMessage.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.Validator
{
    /// <inheritdoc />
    /// <summary>
    ///     The validator has error message.
    /// </summary>
    public interface IValidatorHasErrorMessage<T> : IValidator<T>
    {
        /// <summary>
        ///     The error message.
        /// </summary>
        string ErrorMessage { get; }
    }
}