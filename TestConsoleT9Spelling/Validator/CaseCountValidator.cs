﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CaseCountValidator.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.Validator
{
    /// <summary>
    ///     The case count validator.
    /// </summary>
    public sealed class CaseCountValidator : IValidatorHasErrorMessage<int>
    {
        /// <summary>
        ///     The max value validator.
        /// </summary>
        private readonly IValidator<int> _maxValueValidator;

        /// <summary>
        ///     The min value validator.
        /// </summary>
        private readonly IValidator<int> _minValueValidator;

        /// <inheritdoc />
        public string ErrorMessage { get; private set; } = string.Empty;

        /// <inheritdoc />
        public int ValidationTarget { get; private set; }

        /// <summary>
        ///     Create case count validator.
        /// </summary>
        /// <param name="minValidator">The min case count.</param>
        /// <param name="maxValidator">The max case count.</param>
        public CaseCountValidator(IValidator<int> minValidator, IValidator<int> maxValidator)
        {
            _minValueValidator = minValidator;
            _maxValueValidator = maxValidator;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Validate case count.
        /// </summary>
        /// <param name="count">The case count.</param>
        /// <returns></returns>
        public bool Validate(int count)
        {
            ValidationTarget = count;

            if (!_minValueValidator.Validate(count))
            {
                ErrorMessage = $"The case count be more or equals {_minValueValidator.ValidationTarget}";

                return false;
            }

            if (!_maxValueValidator.Validate(count))
            {
                ErrorMessage = $"The case count must be less or equals {_maxValueValidator.ValidationTarget}";

                return false;
            }

            return true;
        }
    }
}