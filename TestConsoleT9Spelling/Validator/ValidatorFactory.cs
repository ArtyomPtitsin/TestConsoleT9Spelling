﻿namespace TestConsoleT9Spelling.Validator
{
    /// <inheritdoc />
    public sealed class ValidatorFactory : IValidatorFactory
    {
        /// <inheritdoc />
        public IValidator<int> CreateMinValidator(int minValue)
        {
            return new MinValueValidator(minValue);
        }

        /// <inheritdoc />
        public IValidator<int> CreateMaxValidator(int maxValue)
        {
            return new MaxValueValidator(maxValue);
        }

        /// <inheritdoc />
        public IValidatorHasErrorMessage<int> CreateCaseCountValidator(IValidator<int> minValidator, IValidator<int> maxValidator)
        {
            return new CaseCountValidator(minValidator, maxValidator);
        }

        /// <inheritdoc />
        public IValidatorHasErrorMessage<string> CreateMessageValidator(IValidator<int> minValidator, IValidator<int> maxValidator)
        {
            return new MessageValidator(minValidator, maxValidator);
        }
    }
}