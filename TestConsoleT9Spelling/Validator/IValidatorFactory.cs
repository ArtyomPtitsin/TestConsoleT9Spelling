﻿namespace TestConsoleT9Spelling.Validator
{
    /// <summary>
    /// The validator factory.
    /// </summary>
    public interface IValidatorFactory
    {
        /// <summary>
        /// Create min validator.
        /// </summary>
        /// <param name="minValue">The min value.</param>
        /// <returns>The min validator.</returns>
        IValidator<int> CreateMinValidator(int minValue);

        /// <summary>
        /// Create max validator.
        /// </summary>
        /// <param name="maxValue">The max value.</param>
        /// <returns>The max validator.</returns>
        IValidator<int> CreateMaxValidator(int maxValue);

        /// <summary>
        /// Create case count validator.
        /// </summary>
        /// <param name="minValidator">The min case count.</param>
        /// <param name="maxValidator">The max case count.</param>
        /// <returns>The case count validator.</returns>
        IValidatorHasErrorMessage<int> CreateCaseCountValidator(IValidator<int> minValidator, IValidator<int> maxValidator);

        /// <summary>
        /// Create message validator. 
        /// </summary>
        /// <param name="minValidator">The min length of message in characters.</param>
        /// <param name="maxValidator">The max length of message in characters.</param>
        /// <returns>The message validator.</returns>
        IValidatorHasErrorMessage<string> CreateMessageValidator(IValidator<int> minValidator, IValidator<int> maxValidator);
    }
}
