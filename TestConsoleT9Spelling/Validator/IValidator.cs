﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IValidator.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.Validator
{
    /// <summary>
    ///     The validator.
    /// </summary>
    /// <typeparam name="TTarget">Type of the validation target.</typeparam>
    public interface IValidator<TTarget>
    {
        /// <summary>
        ///     Validate value.
        /// </summary>
        /// <param name="validationTarget">The tagret of validation.</param>
        /// <returns>True if validation is success. Otherwise false.</returns>
        bool Validate(TTarget validationTarget);

        /// <summary>
        /// Get validation target.
        /// </summary>
        TTarget ValidationTarget { get; }
    }
}