﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultKeysGenerator.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.Keypad
{
    using System.Collections.Generic;

    /// <inheritdoc />
    public sealed class DefaultKeysGenerator : IKeysGenerator
    {
        /// <inheritdoc />
        public IReadOnlyCollection<IKey> Generate()
        {
            return new List<IKey>
            {
                CreateKey('2', 'a', 'b', 'c'),
                CreateKey('3', 'd', 'e', 'f'),
                CreateKey('4', 'g', 'h', 'i'),
                CreateKey('5', 'j', 'k', 'l'),
                CreateKey('6', 'm', 'n', 'o'),
                CreateKey('7', 'p', 'q', 'r', 's'),
                CreateKey('8', 't', 'u', 'v'),
                CreateKey('9', 'w', 'x', 'y', 'z'),
                CreateKey('0', ' ')
            };
        }

        /// <summary>
        ///     Create key with characters.
        /// </summary>
        /// <param name="key">The char of key.</param>
        /// <param name="characters">The characters.</param>
        /// <returns>The key with characters.</returns>
        private IKey CreateKey(char key, params char[] characters)
        {
            return new Key(key, characters);
        }
    }
}