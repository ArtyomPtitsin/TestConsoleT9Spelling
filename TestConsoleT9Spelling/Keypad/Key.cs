﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Key.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.Keypad
{
    using System.Collections.Generic;

    /// <inheritdoc />
    public class Key : IKey
    {
        /// <inheritdoc />
        public char Value { get; }

        /// <inheritdoc />
        public ICollection<char> Characters { get; }

        /// <summary>
        ///     Create key.
        /// </summary>
        /// <param name="value"> The char of key.</param>
        /// <param name="characters">The characters.</param>
        public Key(char value, ICollection<char> characters)
        {
            Value = value;
            Characters = characters;
        }
    }
}