﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IKeysGenarator.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.Keypad
{
    using System.Collections.Generic;

    /// <summary>
    ///     Keys generator.
    /// </summary>
    public interface IKeysGenerator
    {
        /// <summary>
        ///     Generate keys.
        /// </summary>
        /// <returns>The keys with it characters.</returns>
        IReadOnlyCollection<IKey> Generate();
    }
}