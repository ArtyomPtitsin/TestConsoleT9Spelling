﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IKey.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.Keypad
{
    using System.Collections.Generic;

    /// <summary>
    ///     The key with characters.
    /// </summary>
    public interface IKey
    {
        /// <summary>
        ///     The key code.
        /// </summary>
        char Value { get; }

        /// <summary>
        ///     The characters.
        /// </summary>
        ICollection<char> Characters { get; }
    }
}