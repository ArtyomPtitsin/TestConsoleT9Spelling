﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EDataSet.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling
{
    /// <summary>
    ///     Type of dataset.
    /// </summary>
    public enum EDataSet
    {
        /// <summary>
        ///     A small dataset.
        /// </summary>
        Small,

        /// <summary>
        ///     A large dataset.
        /// </summary>
        Large
    }
}