﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IConsoleUI.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.UI
{
    using System.Collections.Generic;

    /// <summary>
    ///     Console UI for showing tests.
    /// </summary>
    public interface IConsoleUI
    {
        /// <summary>
        ///     Print cases.
        /// </summary>
        /// <param name="messages">The messages.</param>
        void Print(IList<string> messages);
    }
}