﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConsoleUI.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using T9Helper;

    /// <inheritdoc />
    public sealed class ConsoleUI : IConsoleUI
    {
        /// <summary>
        ///     Spacing between column.
        /// </summary>
        private const int SPACING = 2;

        /// <summary>
        ///     Header for the first column.
        /// </summary>
        private const string FIRST_COLUMN_HEADER = "Input";

        /// <summary>
        ///     Header for the second column.
        /// </summary>
        private const string SECOND_COLUMN_HEADER = "Output";

        /// <summary>
        ///     The T9 helper.
        /// </summary>
        private readonly IT9Helper _t9Helper;

        /// <summary>
        ///     Create UI for console with using T9.
        /// </summary>
        /// <param name="t9Helper">The T9 helper.</param>
        public ConsoleUI(IT9Helper t9Helper)
        {
            _t9Helper = t9Helper;
        }

        /// <inheritdoc />
        public void Print(IList<string> messages)
        {
            int realSpacing = CalculateRealSpacing(messages);

            PrintHeaders(realSpacing);
            Console.WriteLine();
            PrintCountCases(messages.Count);
            Console.WriteLine();
            PrintCases(realSpacing, messages);
        }

        /// <summary>
        ///     Calculate the real spacing between the columns.
        /// </summary>
        /// <param name="messages">Messages for determining the real spacing.</param>
        /// <returns>The real spacing beetwen the columns.</returns>
        private int CalculateRealSpacing(IList<string> messages)
        {
            int maxMessageLength = messages.Max(message => message.Length);
            int maxLength = Math.Max(maxMessageLength, FIRST_COLUMN_HEADER.Length);
            int realSpacing = maxLength + SPACING;

            return realSpacing;
        }

        /// <summary>
        ///     Print test cases.
        /// </summary>
        /// <param name="realSpacing">The spacing between column.</param>
        /// <param name="messages">The collection test cases.</param>
        private void PrintCases(int realSpacing, IList<string> messages)
        {
            for (var i = 0; i < messages.Count; i++)
            {
                string message = messages[i];
                Console.Write(message);
                Console.SetCursorPosition(realSpacing, Console.CursorTop);
                Console.Write($"Case #{i}: {_t9Helper.Translate(message)}");
                Console.WriteLine();
            }
        }

        /// <summary>
        ///     Print count test cases.
        /// </summary>
        /// <param name="count">The count test cases.</param>
        private void PrintCountCases(int count)
        {
            Console.Write(count);
        }

        /// <summary>
        ///     Print headers of column.
        /// </summary>
        /// <param name="realSpacing">The spacing between column.</param>
        private void PrintHeaders(int realSpacing)
        {
            Console.Write(FIRST_COLUMN_HEADER);
            Console.SetCursorPosition(realSpacing, 0);
            Console.Write(SECOND_COLUMN_HEADER);
        }
    }
}