﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling
{
    using System;
    using System.Collections.Generic;

    using Keypad;

    using T9Helper;

    using UI;

    using Validator;

    /// <summary>
    ///     Realization of the test task https://code.google.com/codejam/contest/dashboard?c=351101#s=p2
    /// </summary>
    internal class Program
    {
        /// <summary>
        ///     The validators.
        /// </summary>
        private static readonly ValidationHelper ValidationHelper = new ValidationHelper(new ValidatorFactory());

        /// <summary>
        ///     The entry point.
        /// </summary>
        private static void Main()
        {
            IT9Helper t9Helper = CreateT9Helper();
            IConsoleUI consoleUI = CreateConsoleUI(t9Helper);

            int count = ReadCaseCount();
            IList<string> messages = ReadMessages(count, EDataSet.Small);

            Console.Clear();
            consoleUI.Print(messages);
            Console.ReadKey();
        }

        /// <summary>
        ///     Read the case count from console.
        /// </summary>
        /// <returns>The case count</returns>
        private static int ReadCaseCount()
        {
            int count = int.Parse(Console.ReadLine());

            ValidationHelper.ValidateCaseCount(count, 1, 100);

            return count;
        }

        /// <summary>
        ///     Read messages from console.
        /// </summary>
        /// <param name="count">The allowed case count.</param>
        /// <param name="dataSet">Type of dataset.</param>
        /// <returns>The messages.</returns>
        private static IList<string> ReadMessages(int count, EDataSet dataSet)
        {
            var messages = new List<string>(count);
            int minLength;
            int maxLength;

            switch (dataSet)
            {
                case EDataSet.Small:
                    minLength = 1;
                    maxLength = 15;
                    break;
                case EDataSet.Large:
                    minLength = 1;
                    maxLength = 1000;
                    break;
                default:

                    throw new ArgumentOutOfRangeException(nameof(dataSet), dataSet, null);
            }

            for (var i = 0; i < messages.Capacity; i++)
            {
                string message = Console.ReadLine();
                ValidationHelper.ValidateMessage(message, minLength, maxLength);
                messages.Add(message);
            }

            return messages;
        }

        /// <summary>
        ///     Create the console UI.
        /// </summary>
        /// <param name="t9Helper">The t9 helper.</param>
        /// <returns>The console UI.</returns>
        private static IConsoleUI CreateConsoleUI(IT9Helper t9Helper)
        {
            return new ConsoleUI(t9Helper);
        }

        /// <summary>
        ///     Create T9 helper.
        /// </summary>
        /// <returns>The T9 helper.</returns>
        private static IT9Helper CreateT9Helper()
        {
            IReadOnlyCollection<IKey> keys = new DefaultKeysGenerator().Generate();
            Converter<IReadOnlyCollection<IKey>, IReadOnlyDictionary<char, string>> converter =
                new DefaultKeysConverter().Convert;

            return new T9Helper.T9Helper(keys, converter);
        }
    }
}