﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="T9HelperTests.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.T9Helper.Tests
{
    using System;
    using System.Collections.Generic;

    using TestConsoleT9SpellingTests.Infrastructure;

    using Keypad;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class T9HelperTests
    {
        [TestMethod]
        public void TranslateTest_EqualsKeySequance()
        {
            // arrange
            var keysGenerator = new TestKeysGenerator();
            var keysConverter = new TestKeysConverter();
            IReadOnlyCollection<IKey> keys = keysGenerator.Generate();
            const char pause = '#';
            const string message = "bacec";
            var helper = new T9Helper(keys, keysConverter.Convert, pause);

            const string expected = "11#12#222#2";

            // act
            string actual = helper.Translate(message);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TranslateTest_ShouldThrowNotSupportedException()
        {
            // arrange
            var keysGenerator = new TestKeysGenerator();
            var keysConverter = new TestKeysConverter();
            IReadOnlyCollection<IKey> keys = keysGenerator.Generate();
            const char pause = '#';
            const string message = "1";
            var helper = new T9Helper(keys, keysConverter.Convert, pause);

            // act
            Action action = () => helper.Translate(message);

            // assert
            Assert.ThrowsException<NotSupportedException>(action);
        }
    }
}