﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultKeysConverterTests.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.T9Helper.Tests
{
    using System.Collections.Generic;

    using TestConsoleT9SpellingTests.Infrastructure;

    using Keypad;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class DefaultKeysConverterTests
    {
        [TestMethod]
        public void ConvertTest_EqualsMaps()
        {
            // arrange
            var keysGenerator = new TestKeysGenerator();
            IReadOnlyCollection<IKey> keys = keysGenerator.Generate();
            var keysConverter = new DefaultKeysConverter();

            var expected = new Dictionary<char, string>
                {
                    {'a', "1"},
                    {'b', "11"},
                    {'c', "2"},
                    {'d', "22"},
                    {'e', "222"}
                };

            // act
            IReadOnlyDictionary<char, string> actual = keysConverter.Convert(keys);

            // assert
            foreach (KeyValuePair<char, string> actualKeyValuePair in actual)
            {
                CollectionAssert.Contains(expected, actualKeyValuePair);
            }
        }
    }
}