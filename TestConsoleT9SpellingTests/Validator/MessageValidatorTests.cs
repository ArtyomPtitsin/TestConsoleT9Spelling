﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageValidatorTests.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.Validator.Tests
{
    using TestConsoleT9SpellingTests.Infrastructure;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class MessageValidatorTests
    {
        /// <summary>
        ///     The validation factory.
        /// </summary>
        private readonly IValidatorFactory _factory = ValidatorFactory.Factory;

        /// <summary>
        ///     The test validation factory.
        /// </summary>
        private readonly IValidatorFactory _testFactory = ValidatorFactory.TestFactory;

        [TestMethod]
        public void ValidateTest_CharacterLengthIsAllowed()
        {
            // arrange
            const int minCharacterLength = 1;
            const int maxCharacterLength = 1000;
            const string message = "Lorem ipsum dolor sit amet";
            IValidatorHasErrorMessage<string> validator = CreateMessageValidator(minCharacterLength, maxCharacterLength);

            const bool expected = true;
            string expectedErrorMessage = string.Empty;

            // act
            bool actual = validator.Validate(message);
            string actualErrorMessage = validator.ErrorMessage;

            // assert
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expectedErrorMessage, actualErrorMessage);
        }

        [TestMethod]
        public void ValidateTest_CharacterLengthIsNotAllowed_MoreAllowedValue()
        {
            // arrange
            const int minCharacterLength = 1;
            const int maxCharacterLength = 15;
            const string message = "Lorem ipsum dolor sit amet";
            IValidatorHasErrorMessage<string> validator = CreateMessageValidator(minCharacterLength, maxCharacterLength);

            const bool expected = false;
            string expectedErrorMessage = $"The length of message in characters must be less or equals {maxCharacterLength}";

            // act
            bool actual = validator.Validate(message);
            string actualErrorMessage = validator.ErrorMessage;

            // assert
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expectedErrorMessage, actualErrorMessage);
        }

        [TestMethod]
        public void ValidateTest_CharacterLengthIsNotAllowed_LessAllowedValue()
        {
            // arrange
            const int minCharacterLength = 6;
            const int maxCharacterLength = 15;
            const string message = "Lorem";
            IValidatorHasErrorMessage<string> validator = CreateMessageValidator(minCharacterLength, maxCharacterLength);

            const bool expected = false;
            string expectedErrorMessage = $"The length of message in characters must be more or equals {minCharacterLength}";

            // act
            bool actual = validator.Validate(message);
            string actualErrorMessage = validator.ErrorMessage;

            // assert
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expectedErrorMessage, actualErrorMessage);
        }

        /// <summary>
        ///     Create message validator.
        /// </summary>
        /// <param name="minCharacterLength">The min value of case count.</param>
        /// <param name="maxCharacterLength">The max value of case count.</param>
        /// <returns>The validator.</returns>
        private IValidatorHasErrorMessage<string> CreateMessageValidator(int minCharacterLength, int maxCharacterLength)
        {
            IValidator<int> minValueValidator = _testFactory.CreateMinValidator(minCharacterLength);
            IValidator<int> maxValueValidator = _testFactory.CreateMaxValidator(maxCharacterLength);

            return _factory.CreateMessageValidator(minValueValidator, maxValueValidator);
        }
    }
}