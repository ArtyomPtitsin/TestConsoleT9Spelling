﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MinValueValidatorTests.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.Validator.Tests
{
    using TestConsoleT9SpellingTests.Infrastructure;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class MinValueValidatorTests
    {
        /// <summary>
        /// The validation factory.
        /// </summary>
        private readonly IValidatorFactory _factory = ValidatorFactory.Factory;

        [TestMethod]
        public void ValidateTest_ValueMoreMinValue()
        {
            // arrange
            const int value = 5;
            const int minValue = 1;
            var validator = _factory.CreateMinValidator(minValue);

            const bool expected = true;

            // act
            bool actual = validator.Validate(value);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ValidateTest_ValueEqualsMinValue()
        {
            // arrange
            const int value = 1;
            const int minValue = 1;
            var validator = _factory.CreateMinValidator(minValue);

            const bool expected = true;

            // act
            bool actual = validator.Validate(value);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ValidateTest_ValueLessMinValue()
        {
            // arrange
            const int value = 1;
            const int minValue = 5;
            var validator = _factory.CreateMinValidator(minValue);

            const bool expected = false;

            // act
            bool actual = validator.Validate(value);

            // assert
            Assert.AreEqual(expected, actual);
        }
    }
}