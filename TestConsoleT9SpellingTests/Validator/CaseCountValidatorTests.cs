﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CaseCountValidatorTests.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.Validator.Tests
{
    using TestConsoleT9SpellingTests.Infrastructure;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class CaseCountValidatorTests
    {
        /// <summary>
        ///     The validation factory.
        /// </summary>
        private readonly IValidatorFactory _factory = ValidatorFactory.Factory;

        /// <summary>
        ///     The test validation factory.
        /// </summary>
        private readonly IValidatorFactory _testFactory = ValidatorFactory.TestFactory;

        [TestMethod]
        public void ValidateTest_CaseCountIsAllowed()
        {
            // arrange
            const int minCount = 1;
            const int maxCount = 100;
            const int count = 50;
            IValidatorHasErrorMessage<int> validator = CreateCaseCountValidator(minCount, maxCount);

            const bool expected = true;
            string expectedErrorMessage = string.Empty;

            // act
            bool actual = validator.Validate(count);
            string actualErrorMessage = validator.ErrorMessage;

            // assert
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expectedErrorMessage, actualErrorMessage);
        }

        [TestMethod]
        public void ValidateTest_CaseCountIsNotAllowed_LessAllowedValue()
        {
            // arrange
            const int minCount = 5;
            const int maxCount = 100;
            const int count = 1;
            IValidatorHasErrorMessage<int> validator = CreateCaseCountValidator(minCount, maxCount);

            const bool expected = false;
            string expectedErrorMessage = $"The case count be more or equals {minCount}";

            // act
            bool actual = validator.Validate(count);
            string actualErrorMessage = validator.ErrorMessage;

            // assert
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expectedErrorMessage, actualErrorMessage);
        }

        [TestMethod]
        public void ValidateTest_CaseCountIsNotAllowed_MoreAllowedValue()
        {
            // arrange
            const int minCount = 1;
            const int maxCount = 100;
            const int count = 1000;
            IValidatorHasErrorMessage<int> validator = CreateCaseCountValidator(minCount, maxCount);

            const bool expected = false;
            string expectedErrorMessage = $"The case count must be less or equals {maxCount}";

            // act
            bool actual = validator.Validate(count);
            string actualErrorMessage = validator.ErrorMessage;

            // assert
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expectedErrorMessage, actualErrorMessage);
        }

        /// <summary>
        ///     Create case count validator.
        /// </summary>
        /// <param name="minCount">The min value of case count.</param>
        /// <param name="maxCount">The max value of case count.</param>
        /// <returns>The validator.</returns>
        private IValidatorHasErrorMessage<int> CreateCaseCountValidator(int minCount, int maxCount)
        {
            IValidator<int> minValueValidator = _testFactory.CreateMinValidator(minCount);
            IValidator<int> maxValueValidator = _testFactory.CreateMaxValidator(maxCount);

            return _factory.CreateCaseCountValidator(minValueValidator, maxValueValidator);
        }
    }
}