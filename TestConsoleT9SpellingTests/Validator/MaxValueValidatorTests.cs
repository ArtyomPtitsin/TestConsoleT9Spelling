﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MaxValueValidatorTests.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9Spelling.Validator.Tests
{
    using TestConsoleT9SpellingTests.Infrastructure;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class MaxValueValidatorTests
    {
        /// <summary>
        ///     The validation factory.
        /// </summary>
        private readonly IValidatorFactory _factory = ValidatorFactory.Factory;

        [TestMethod]
        public void ValidateTest_ValueLessMaxValue()
        {
            // arrange
            const int value = 1;
            const int maxValue = 5;
            IValidator<int> validator = _factory.CreateMaxValidator(maxValue);

            const bool expected = true;

            // act
            bool actual = validator.Validate(value);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ValidateTest_ValueEqualsMaxValue()
        {
            // arrange
            const int value = 1000;
            const int maxValue = 1000;
            IValidator<int> validator = _factory.CreateMaxValidator(maxValue);

            const bool expected = true;

            // act
            bool actual = validator.Validate(value);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ValidateTest_ValueMoreMaxValue()
        {
            // arrange
            const int value = 5;
            const int maxValue = 1;
            IValidator<int> validator = _factory.CreateMaxValidator(maxValue);

            const bool expected = false;

            // act
            bool actual = validator.Validate(value);

            // assert
            Assert.AreEqual(expected, actual);
        }
    }
}