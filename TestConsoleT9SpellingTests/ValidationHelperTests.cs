﻿namespace TestConsoleT9Spelling.Tests
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Validator;

    using ValidatorFactory = TestConsoleT9SpellingTests.Infrastructure.ValidatorFactory;

    [TestClass]
    public class ValidationHelperTests
    {
        /// <summary>
        ///     The test validation factory.
        /// </summary>
        private readonly IValidatorFactory _testFactory = ValidatorFactory.TestFactory;

        [TestMethod]
        public void ValidateCaseCountTest_ShouldThrowArgumentOutOfRangeException()
        {
            // arrange
            const int count = 100000;
            const int minCount = 1;
            const int maxCount = 100;
            var validators = new ValidationHelper(_testFactory);

            // act
            Action action = () => validators.ValidateCaseCount(count, minCount, maxCount);

            // assert
            Assert.ThrowsException<ArgumentOutOfRangeException>(action);
        }

        [TestMethod]
        public void ValidateMessageTest_ShouldThrowArgumentOutOfRangeExceptionWhenSmallData()
        {
            // arrange
            const int minLength = 1;
            const int maxLength = 15;
            const string message = "Lorem ipsum dolor sit amet";
            var validators = new ValidationHelper(_testFactory);

            // act
            Action action = () => validators.ValidateMessage(message, minLength, maxLength);

            // assert
            Assert.ThrowsException<ArgumentOutOfRangeException>(action);
        }
    }
}