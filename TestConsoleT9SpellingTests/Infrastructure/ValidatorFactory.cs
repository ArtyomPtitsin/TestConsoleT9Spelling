﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorFactory.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9SpellingTests.Infrastructure
{
    using TestConsoleT9Spelling.Validator;

    /// <summary>
    ///     The validator factory.
    /// </summary>
    internal class ValidatorFactory
    {
        /// <summary>
        ///     Get validator factory.
        /// </summary>
        public static IValidatorFactory Factory
        {
            get => new TestConsoleT9Spelling.Validator.ValidatorFactory();
        }

        /// <summary>
        ///     Get test validator factory.
        /// </summary>
        public static IValidatorFactory TestFactory
        {
            get => new TestConsoleT9Spelling.Validator.ValidatorFactory();
        }
    }
}