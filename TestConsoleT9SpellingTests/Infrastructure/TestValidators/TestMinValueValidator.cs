﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MinValueValidator.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9SpellingTests.Infrastructure.TestValidators
{
    using TestConsoleT9Spelling.Validator;

    /// <summary>
    ///     The min value validator.
    /// </summary>
    internal class TestMinValueValidator : IValidator<int>
    {
        /// <inheritdoc />
        public int ValidationTarget { get; }

        /// <summary>
        ///     Create min value validator.
        /// </summary>
        /// <param name="minValue">The min value.</param>
        public TestMinValueValidator(int minValue)
        {
            ValidationTarget = minValue;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Validate value.
        /// </summary>
        /// <param name="value">The value for validate.</param>
        /// <returns>If value less or equals then true. Otherwise false.</returns>
        public bool Validate(int value)
        {
            return value >= ValidationTarget;
        }
    }
}