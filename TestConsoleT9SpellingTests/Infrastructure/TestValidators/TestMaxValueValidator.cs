﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MaxValueValidator.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9SpellingTests.Infrastructure.TestValidators
{
    using TestConsoleT9Spelling.Validator;

    /// <summary>
    ///     The max value validator.
    /// </summary>
    internal class TestMaxValueValidator : IValidator<int>
    {
        /// <inheritdoc />
        public int ValidationTarget { get; }

        /// <summary>
        ///     Create max value validator.
        /// </summary>
        /// <param name="maxValue">The max value.</param>
        public TestMaxValueValidator(int maxValue)
        {
            ValidationTarget = maxValue;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Validate value.
        /// </summary>
        /// <param name="value">The value for validate.</param>
        /// <returns>If value more or equals then true. Otherwise false.</returns>
        public bool Validate(int value)
        {
            return value <= ValidationTarget;
        }
    }
}