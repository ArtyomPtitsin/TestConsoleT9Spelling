﻿namespace TestConsoleT9SpellingTests.Infrastructure.TestValidators
{
    using TestConsoleT9Spelling.Validator;

    /// <inheritdoc />
    internal class TestValidatorFactory : IValidatorFactory
    {
        /// <inheritdoc />
        public IValidator<int> CreateMinValidator(int minValue)
        {
            return new TestMinValueValidator(minValue);
        }

        /// <inheritdoc />
        public IValidator<int> CreateMaxValidator(int maxValue)
        {
            return new TestMaxValueValidator(maxValue);
        }

        /// <inheritdoc />
        public IValidatorHasErrorMessage<int> CreateCaseCountValidator(IValidator<int> minValidator, IValidator<int> maxValidator)
        {
            return new TestCaseCountValidator(minValidator, maxValidator);
        }

        /// <inheritdoc />
        public IValidatorHasErrorMessage<string> CreateMessageValidator(IValidator<int> minValidator, IValidator<int> maxValidator)
        {
            return new TestMessageValidator(minValidator, maxValidator);
        }
    }
}