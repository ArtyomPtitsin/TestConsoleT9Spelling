﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageValidator.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9SpellingTests.Infrastructure.TestValidators
{
    using TestConsoleT9Spelling.Validator;

    /// <summary>
    ///     The message validator.
    /// </summary>
    internal class TestMessageValidator : IValidatorHasErrorMessage<string>
    {
        /// <summary>
        ///     The max value validator.
        /// </summary>
        private readonly IValidator<int> _maxValueValidator;

        /// <summary>
        ///     The min value validator.
        /// </summary>
        private readonly IValidator<int> _minValueValidator;

        /// <inheritdoc />
        public string ErrorMessage { get; private set; }

        /// <inheritdoc />
        public string ValidationTarget { get; private set; } = string.Empty;

        /// <summary>
        ///     Create message validator.
        /// </summary>
        /// <param name="minValidator">The min length of message in characters.</param>
        /// <param name="maxValidator">The max length of message in characters.</param>
        public TestMessageValidator(IValidator<int> minValidator, IValidator<int> maxValidator)
        {
            _minValueValidator = minValidator;
            _maxValueValidator = maxValidator;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Valildate message.
        /// </summary>
        /// <param name="message">The message for validate.</param>
        /// <returns>True if validation is success. Otherwise false.</returns>
        public bool Validate(string message)
        {
            ValidationTarget = message;

            if (!_minValueValidator.Validate(message.Length))
            {
                ErrorMessage = "MinTestFailed";

                return false;
            }

            if (!_maxValueValidator.Validate(message.Length))
            {
                ErrorMessage = "MaxTestFailed";

                return false;
            }

            return true;
        }
    }
}