﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestKeysGenerator.cs" company="Artyom Ptitsin">
//    Artyom Ptitsin (c)
// </copyright>
// <summary>
//    Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TestConsoleT9SpellingTests.Infrastructure
{
    using System.Collections.Generic;

    using TestConsoleT9Spelling.Keypad;

    internal class TestKeysGenerator : IKeysGenerator
    {
        /// <inheritdoc />
        public IReadOnlyCollection<IKey> Generate()
        {
            return new List<IKey> {CreateKey('1', 'a', 'b'), CreateKey('2', 'c', 'd', 'e')};
        }

        /// <summary>
        ///     Create key with characters.
        /// </summary>
        /// <param name="key">The char of key.</param>
        /// <param name="characters">The characters.</param>
        /// <returns>The key with characters.</returns>
        private IKey CreateKey(char key, params char[] characters)
        {
            return new Key(key, characters);
        }
    }
}